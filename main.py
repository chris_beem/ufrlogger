from helper import *
from ssh_logger import *

current_dir = os.path.dirname(os.path.abspath(__file__))

# print(sc_notation(1236e-4+1.000000000001))
# 0/0
# import datetime
# a = datetime.datetime(2020,9,29)
# sleep(0.1)
# b = get_current_time()
#
# print(get_time_difference(a,b['value']))
# 0/0

longopts = ['ip=', 'name=', 'password=', 'speriod=', 'nsamples=', 'th1=', 'thn=', 'indefinite_mode=']

def get_input_options():
    argv = sys.argv[1:]
    opts, args = getopt.getopt(argv, 'a:b:c', longopts)
    return opts


opts = get_input_options()
port = 80

server0 = ["10.1.200.33"]
username0 = ["vscli"]
sample_period0 = 1
n_samples0 = 12
UFR_short_TH0 = [0.00001]
UFR_long_TH0 = [0.00001]
passwords0 = ["vscli"]
mmode0 = MMODE_I

for op in opts:
    if op[0] == '--ip':
        server0 = op[1].split(',')
    if op[0] == '--name':
        username0 = op[1].split(',')
    if op[0] == '--password':
        passwords0 = op[1].split(',')
    if op[0] == '--speriod':
        sample_period0 = int(op[1])
    if op[0] == '--nsamples':
        n_samples0 = int(op[1])
    if op[0] == '--th1':
        UFR_short_TH0 = [float(o) for o in op[1].split(',')]
    if op[0] == '--thn':
        UFR_long_TH0 = [float(o) for o in op[1].split(',')]
    if op[0] == '--indefinite_mode':
        if int(op[1]):
            mmode0 = MMODE_I
        else:
            mmode0 = MMODE_D

print("UFR LOGGER v2.1\nBy Chris beem\nARGUMENT OPTIONS {}\r\n".format(longopts))
print(f"USERNAME: {username0}, SERVERNAME: {server0}, sleep(sec): {sample_period0}")
print(f"UFR threshold {sample_period0}sec : {UFR_short_TH0}, UFR threshold {n_samples0}sec : {UFR_long_TH0}\r\n\r\n")

class Root:
    def __init__(self):
        self.number_of_servers = 0  # len(server)
        self.pws = [False for i in range(self.number_of_servers)]
        self.tables = [0 for i in range(self.number_of_servers)]

        self.log_start_time = get_current_time()
        self.last_update = get_current_time()

        self.SETUP_PAGE = open("media/html/setup.html").read()
        self.MAIN_PAGE = open("media/html/main.html").read()
        self.FRAME_PAGE = open("media/html/frame.html").read()
        self.GLOBAL_FRAME_PAGE = open("media/html/frame_global.html").read()

        self.queues = [{'M>L': Queue(), 'L>M': Queue(), 'service': Queue()} for i in range(32)]
        self.system_info = [{'cm_sn': 'unknown', 'cm_sv': 'unknown', 'samples_taken': 'unknown', 'ip':'unknown', 'verdict':'..busy..'} for i in
                            range(32)]  # max number of servers, now 32, should make variable

        self.servers = server0
        self.usernames = username0
        self.passwords = passwords0
        self.sample_period = sample_period0
        self.n_samples = n_samples0
        self.UFR_short_TH = UFR_short_TH0
        self.UFR_long_TH = UFR_long_TH0
        self.mmode = mmode0

        self.events = []

        self.logger_threads = []
        self.inited = False

    def start_SSHLOGGER_trhreads(self, servers, usernames, passwords, sample_period, n_samples, ufr_s, ufr_l, mmode):

        self.number_of_servers = len(servers)
        self.pws = [False for i in range(self.number_of_servers)]
        self.tables = [0 for i in range(self.number_of_servers)]
        self.queues = [{'M>L': Queue(), 'L>M': Queue(), 'service': Queue()} for i in
                       range(self.number_of_servers)]  # ssh_logger to main
        self.system_info = [{'cm_sn': 'unknown', 'cm_sv': 'unknown'} for i in
                            range(self.number_of_servers)]  # ssh_logger to main

        self.events = [threading.Event() for i in range(self.number_of_servers)]

        for i, s in enumerate(servers):
            #("Starting Thread ", s, usernames[i], sample_period, n_samples, ufr_s[i], ufr_l[i])
            self.logger_threads.append(threading.Thread(target=SSH_LOGGER_STARTER, daemon=True, args=(self.queues[i],
                                                                                              s,
                                                                                              usernames[i],
                                                                                              passwords[i],
                                                                                              sample_period,
                                                                                              n_samples,
                                                                                              ufr_s[i],
                                                                                              ufr_l[i],
                                                                                              mmode,
                                                                                              self.events[i])))
            self.logger_threads[-1].start()
        self.inited = True

    @cherrypy.expose
    def JSONget_setup(self):

        ret = {"servers": self.servers,
               "usernames": self.usernames,
               "passwords": self.passwords,
               "sample_period": self.sample_period,
               "n_samples": self.n_samples,
               "UFR_short_THs": self.UFR_short_TH,
               "UFR_long_THs": self.UFR_long_TH,
               "mmode": self.mmode,
               "inited": self.inited}

        return json.dumps(ret)

    @cherrypy.expose
    def JSONset_setup(self, servers, usernames, passwords, sample_period, n_samples, UFR_short_TH, UFR_long_TH, mmode):

        self.servers = list(filter(None, json.loads(servers)))
        self.usernames = json.loads(usernames)
        self.passwords = json.loads(passwords)
        self.sample_period = json.loads(sample_period)
        self.n_samples = json.loads(n_samples)
        self.UFR_short_TH = json.loads(UFR_short_TH)
        self.UFR_long_TH = json.loads(UFR_long_TH)
        self.mmode = json.loads(mmode)

        # print(self.servers, self.usernames, self.passwords, self.sample_period,
        #       self.n_samples, self.UFR_short_TH, self.UFR_long_TH, mmode)

        self.start_SSHLOGGER_trhreads(servers=self.servers,
                                      usernames=self.usernames,
                                      passwords=self.passwords,
                                      sample_period=self.sample_period,
                                      n_samples=self.n_samples,
                                      ufr_s=self.UFR_short_TH,
                                      ufr_l=self.UFR_long_TH,
                                      mmode=self.mmode)


        self.inited = True
        return json.dumps("oke")

    @cherrypy.expose
    def JSONget_global_system_information(self):
        now = get_current_time()
        starttime_val = self.log_start_time['value']
        starttime_str = self.log_start_time['str']
        uptime = get_time_difference(starttime_val, now['value'])

        servers = self.servers
        sample_period = self.sample_period
        n_samples = self.n_samples
        mmode = self.mmode

        time_info = {'starttime': starttime_str,
                     'uptime': str(uptime),
                     'servers':servers,
                     'sample_period':sample_period,
                     'n_samples':n_samples,
                     'mmode':mmode,
                     'th1':self.UFR_long_TH,
                     'thn':self.UFR_long_TH}

        return json.dumps(time_info)

    @cherrypy.expose
    def JSONget_systeminfo(self):
        for i in range(len(self.queues)):
            queue = self.queues[i]['service']
            while queue.qsize() > 0:
                data = queue.get()
                self.system_info[i]['cm_sn'] = str(data['cm_sn'])
                self.system_info[i]['cm_sv'] = str(data['cm_sv'])
                self.system_info[i]['samples_taken'] = str(data['samples_taken'])
                self.system_info[i]['ip'] = self.servers[i]
                self.system_info[i]['verdict'] = str(data['verdict'])

        return json.dumps(self.system_info)



    @cherrypy.expose
    def default(self):
        iframe = open("media/html/iframe.html").read()
        iframes = ''.join([iframe.format(server=s, frame_nr=i) for i, s in enumerate(server0)])
        page = self.MAIN_PAGE.replace('{iframes}', iframes)  # format(iframes=iframes)
        return page

    @cherrypy.expose
    def frame(self, i=-1):
        if self.inited == False:
            return "..."

        i = int(i)

        while (self.queues[i]['L>M'].qsize() > 0):
            dct = self.queues[i]['L>M'].get()
            self.tables[i] = dct_to_html(dct)
            self.last_update = get_current_time()

        last_update_delta = get_time_difference(self.last_update['value'], get_current_time()['value'])

        return self.FRAME_PAGE.format(last_update=self.last_update['str'],
                                      last_update_delta=f"({last_update_delta} ago)",
                                      table=self.tables[i],
                                      table_setup="""table, th, td {border: 1px solid black;}""",
                                      )

    @cherrypy.expose
    def update_frame_global(self):
        now = get_current_time()
        difference = get_time_difference(self.log_start_time['value'], now['value'])
        frame = self.GLOBAL_FRAME_PAGE
        frame = frame.format(file_references=get_HREFs_from_folder("media\logs"), )
        return frame

    @cherrypy.expose
    def get_log_file_table(self):
        ret = os.listdir('media/logs/')
        return json.dumps(ret)

    @cherrypy.expose
    def get_password_state(self, server_id):
        server_id = json.loads(server_id)
        return json.dumps(self.pws[server_id])

    @cherrypy.expose
    def set_password_state(self, server_id, pw):
        server_id = int(json.loads(server_id))
        self.pws[server_id] = str(pw)
        self.self.queues[server_id]['M>L'].put({"PASSWORD": pw})
        return json.dumps(self.pws)

    @cherrypy.expose
    def JSONset_restart(self):
        #print(sys.executable,',[main.exe],',sys.argv)
        #sleep(10)
        #os.system('cls')
        print(C_ALARM + "\r\n\r\nATTEMPTING TO RESTART APPLICATION......\r\n\r\n" + C_END)
        try:
            os.execv('main.exe', sys.argv)
        except Exception as e:
            print(C_ALARM + f"\r\n\r\nCOULD NOT RESTART! \r\n{e}\r\n\r\n" + C_END)
            return json.dumps('restart failed.. '+str(e))

config = {
    'global': {
        'server.socket_host': '127.0.0.1',
        'server.socket_port': port,
        'server.thread_pool': 8,
        'log.screen': False
    },
    '/media': {'tools.staticdir.on': True,
               'tools.staticdir.dir': os.path.join(current_dir, 'media'),
               'tools.staticdir.content_types': {'txt': ""}}
}

if __name__ == "__main__":
    while(1):
        try:
            print("START WEB SERVER")
            print(
                f"\n Goto {config['global']['server.socket_host']}:{config['global']['server.socket_port']} for display page \n")
            Root().frame()
            cherrypy.config.update(config=config)
            cherrypy.quickstart(Root(), '/', config=config)
            print("restarting...")
        except:
            print(".......")
            sleep(5)
