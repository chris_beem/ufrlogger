@echo "Closing application still running..."
@echo off
@TITLE Starting TECHNETIX-UFR-LOGGER

@taskkill /F /FI "WindowTitle eq Technetix-UFR-logger" /T

@timeout 3

@TITLE Technetix-UFR-logger
@echo on
:: start http://localhost
main.exe --ip=10.1.200.32,10.1.200.33 --name=vscli --password=vscli --speriod=10 nsamples=3 --th1=0.01 --thn=0.02 --indefinite_mode=0


@echo "closing application"
@timeout 3
:: cmd /k