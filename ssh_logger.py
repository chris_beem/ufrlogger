from time import sleep
from datetime import datetime
from queue import Queue
from helper import *

os.system("")

password_default = "vscli"
cmd_gci = 'get vs connection info\n'
cmd_gsi = 'get system info\n'


class SSH():
    def __init__(self):
        self.ssh = paramiko.SSHClient()

    def open_ssh(self, server, username, password, port=22):
        print("Opening SSH connection {}@{}".format(username, server))
        self.ssh = paramiko.SSHClient()
        self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.ssh.connect(hostname=server, port=port, username=username, password=password)
        stdin = self.ssh.exec_command("")

    def send_cmd(self, cmd, sleep_after=5):
        remote_conn = self.ssh.invoke_shell()
        remote_conn.send(cmd)
        print('collecting data from nodes...')
        sleep(sleep_after)
        output = str(remote_conn.recv(-1), encoding='utf8')
        return output

    def is_alive(self):
        try:
            transport = self.ssh.get_transport()
            transport.send_ignore()
            return 1
        except:
            self.ssh = paramiko.SSHClient()
            return 0
    # connection is closed


def parse_node_info(str):
    line_split_char = '\r\n'
    node_data = [[line for line in node_text.split(line_split_char)] for node_text in str.split('\r\n\r\n') if
                 "Description" in node_text]

    array_out = []
    for node in node_data:
        array_out.append({})
        for line in node:
            splits = line.split(':')
            if len(splits) == 2:
                key = splits[0]
                key = key.replace(' ', '')

                val = splits[1]
                val = val.replace(' ', '')
                try:
                    val = int(val)
                except:
                    pass

                array_out[-1].update({'{}'.format(key): val})


    dct_out = {f"INDEX_{index}_NAME_{array['Description']}": array
               for index,array in enumerate(array_out) if 'Description' in array} #each element (node) has a unique descriptor now

    return dct_out


class SSH_LOGGER_STARTER():
    def __init__(self,
                 queues,
                 server="sxxx",
                 username="uxxx",
                 password="pxxx",
                 sample_period=1,
                 n_samples=12,
                 UFR_short_TH=0.001,
                 UFR_long_TH=0.0003,
                 mmode=MMODE_I,
                 thread_event=threading.Event()):
        while(1):
            try:
                SSH_LOGGER(queues=queues,
                           server=server,
                           username=username,
                           password=password,
                           sample_period=sample_period,
                           n_samples=n_samples,
                           UFR_long_TH=UFR_long_TH,
                           UFR_short_TH=UFR_short_TH,
                           mmode=mmode)
            except Exception as e:
                queues['L>M'].put({'ID':{
                    "ERROR!!": color_html_element("Thread crashed", "red"),
                    "Reason:": str(e),
                }})
                time.sleep(15)

            time.sleep(1)

class SSH_LOGGER():
    def __init__(self,
                 queues,
                 server="sxxx",
                 username="uxxx",
                 password="pxxx",
                 sample_period=1,
                 n_samples=12,
                 UFR_short_TH=0.001,
                 UFR_long_TH=0.0003,
                 mmode=MMODE_I):

        self.SI_serialnumber = "xxx"  # CM system_information
        self.SI_softwareversion = "xxx"  # CM system_information
        self.samples_taken = 0  # how many samples were succesfully taken from the CM connection info

        # print(queues)

        self.queue_out = queues['L>M']
        self.queue_in = queues['M>L']
        self.queue_state_info = queues['service']

        self.server = server
        self.username = username
        self.password = password
        self.sample_period = float(sample_period)
        self.n_samples = float(n_samples)
        self.UFR_short_TH = float(UFR_short_TH)
        self.UFR_long_TH = float(UFR_long_TH)
        self.mmode = mmode
        self.filename = f"media\\logs\\ALARMS@{server}.txt"
        self.HTML_output_dict = {}
        self.cumulative_UFR = {'US_short': 0, 'DS_short': 0, 'US_long': 0, "US_lowest_l1max": 999999,
                               'DS_long': 0, 'US_forever': 0, 'DS_forever': 0, "DS_lowest_l1max": 999999}

        self.alarms = [{'UFR_short': False, 'UFR_long': False, 'index': i, 'name': '___', 'final_verdict':VERDICT.UNKNOWN} for i in range(256)]

        self.verdict = VERDICT.UNKNOWN
        self.verdict_str = '...'
        self.verdict_cause_str = '...'

        self.ssh = SSH()
        self.sshout_dict = {}
        self.connection_info_dict = {}

        self.log_message(f"--Logging of {server} VSCM UFR started at {get_current_time()['str']}--\n")
        self.run()

    def run(self):
        while (1):
            sleep(1)
            self.HTML_output_dict = {}
            start_time = get_current_time()['str']
            print(C_HEADER + f"#############################################" + C_END)
            print(C_HEADER + f"~~~~Connection Info From {self.server} ({start_time})~~~~" + C_END)
            print(C_HEADER + f"#############################################" + C_END)

            self.sshout_dict = self.get_data_over_ssh()
            if self.verdict!=VERDICT.UNKNOWN: #verdict has been cast
                print(f"VERDICT HAS BEEN CAST FOR {self.server}! \r\n{self.verdict_str}, \r\n{self.verdict_cause_str}\r\n\r\n")
            elif self.sshout_dict:
                self.add_UFR_info_to_data()
                self.set_alarms_to0()

                summary = []

                for server_index, node_info in enumerate(self.connection_info_dict.values()):

                    name = node_info['Description']
                    Direction = node_info['Direction']
                    L1Max = float(node_info['L1Max(Mbps)'])
                    status = node_info['Status']
                    lost_now, lost_past, lost_hour = node_info['lost hist'][0], node_info['lost hist'][1], node_info['lost hist'][-1]  # note
                    total_now, total_past, total_hour = node_info['total hist'][0], node_info['total hist'][1], node_info['total hist'][
                        -1]  # MUST LOOK IF THIS IS THE SAME AS 11!

                    Direction_COLOR = DIR_COL_US if Direction == 'upstream' else DIR_COL_DS

                    UFR_Forever = lost_now / (0.001 + total_now)
                    UFR_short, UFR_long = 0, 0

                    if total_now != total_past:
                        UFR_short = (lost_now - lost_past) / (0.001 + total_now - total_past)

                    if total_now != total_hour:
                        UFR_long = (lost_now - lost_hour) / (0.001 + total_now - total_hour)

                    if (total_now == total_past) and (lost_now != lost_past):
                        UFR_short = 1

                    if (total_now == total_hour) and (lost_now != lost_hour):
                        UFR_long = 1

                    ufs, ufl = self.check_for_UFR_alarms(name, UFR_short, UFR_long)

                    self.alarms[server_index]['UFR_short'] = ufs
                    self.alarms[server_index]['UFR_long'] = ufl
                    self.alarms[server_index]['name'] = name
                    self.alarms[server_index]['index'] = server_index

                    print(f"\r\n\r\t{server_index})->--------name: {name} @ {self.server}-------")

                    print(f"""
                           LOST PACKS NOW: {lost_now} 
                           LOST PACKS {self.sample_period}  sec ago: {lost_past}
                           LOST PACKS {self.n_samples} sec ago: {lost_hour}
                           (delta = {lost_now - lost_past},{lost_now - lost_hour})""")

                    print(f"""
                           TOTAL PACKS NOW: {total_now} 
                           TOTAL PACKS {self.sample_period}  sec ago: {total_past}
                           TOTAL PACKS {self.n_samples} sec ago: {total_hour}
                           (delta = {total_now - total_past},{total_now - total_hour})""")

                    print(
                        f"\r\nUFR SHORT,LONG,FOREVER: {[sc_notation(UFR_short), sc_notation(UFR_long), sc_notation(UFR_Forever)]} \r\n")

                    if self.alarms[server_index]['UFR_long'] or self.alarms[server_index]['UFR_short']:
                        summary.append(["!!!Alarm for {}".format(name)])

                    self.append_queue_message({
                        "NAME": color_html_element(name, "green"),
                        "STATUS": status,
                        "Direction": Direction,
                        "L1 Max": L1Max,
                        "UFR SHORT": color_html_element(sc_notation(UFR_short),
                                                        "red" if self.alarms[server_index]['UFR_short'] else "black"),
                        "UFR LONG ": color_html_element(sc_notation(UFR_long),
                                                        "red" if self.alarms[server_index]['UFR_long'] else "black"),
                        "UFR forever": sc_notation(UFR_Forever)
                        ,
                    }, send=False, name=f"""{name} style="background-color:{Direction_COLOR}" """)

                    self.update_cumulative_UFR(Direction, UFR_short, UFR_long, UFR_Forever, L1Max)

                self.append_queue_message({
                    "TOTAL US/DS": color_html_element("TOTAL US", "green"),
                    " ": " ",
                    "  ": " ",
                    "Lowest L1 Max": self.cumulative_UFR['US_lowest_l1max'],
                    "TOTAL UFR SHORT": sc_notation(self.cumulative_UFR['US_short']),
                    "TOTAL UFR  LONG": sc_notation(self.cumulative_UFR['US_long']),
                    "TOTAL UFR forever": sc_notation(self.cumulative_UFR['US_forever']),

                }, send=False, name=f"""TOTAL US style="background-color:{DIR_COL_US}" """)

                self.append_queue_message({
                    "TOTAL US/DS": color_html_element("TOTAL DS", "green"),
                    " ": " ",
                    "  ": " ",
                    "Lowest L1 Max": self.cumulative_UFR['DS_lowest_l1max'],
                    "TOTAL UFR SHORT": sc_notation(self.cumulative_UFR['DS_short']),
                    "TOTAL UFR  LONG": sc_notation(self.cumulative_UFR['DS_long']),
                    "TOTAL UFR forever": sc_notation(self.cumulative_UFR['DS_forever']),
                }, send=False, name=f"""TOTAL DS style="background-color:{DIR_COL_DS}" """)

                self.reset_cumulative_UFR()

                for s in summary:
                    print(C_ALARM + str(s) + C_END)

                self.samples_taken += 1

                self.check_outcome()

                self.queue_out.put(self.HTML_output_dict)

            else:
                print(".<ERROR> NO DATA PARSED.")


            if self.mmode == MMODE_D:
                samples_taken_state = str( f"samples taken: {self.samples_taken} out of {self.n_samples} (taken every {self.sample_period} seconds)")
            else: ##INFINITE MODEs
                samples_taken_state = str(
                    f"samples taken: {self.samples_taken} (taken every {self.sample_period} seconds)")

            self.queue_state_info.put({'cm_sn': self.SI_serialnumber,
                                       'cm_sv': self.SI_softwareversion,
                                       'samples_taken': samples_taken_state,
                                       'verdict':f"{self.verdict_str} \r\n {self.verdict_cause_str}"})

            sleep(self.sample_period)

    def check_outcome(self):
        outcome = '.'
        cause = '.'
        if self.mmode == MMODE_I:
            outcome = color_html_element('no outcome ever','green')
            cause = color_html_element('Using continuous mode, (continuous measurements without verdict)','green')

        if self.mmode == MMODE_D:
            if (self.samples_taken >= self.n_samples) and (self.verdict == VERDICT.UNKNOWN):
                self.verdict = VERDICT.PASS

                for server_index in range(len(self.alarms)):
                        if (self.alarms[server_index]['UFR_long'] == True):
                            self.verdict = VERDICT.FAIL
                            self.alarms[server_index]['final_verdict'] = VERDICT.FAIL

            offender_names = list(filter(None,[a['name'] if a['final_verdict'] == VERDICT.FAIL else '' for a in self.alarms]))

            if self.verdict == VERDICT.PASS:
                outcome = color_html_element("PASSED!", "green",BACKGROUND=True)
                cause = f" UFR over {self.n_samples} measurements has NOT exceeded the UFR threshold of {self.UFR_long_TH}"

            if self.verdict == VERDICT.FAIL:
                outcome = color_html_element("FAIL!", "red",BACKGROUND=True)
                cause = color_html_element(f" UFR over {self.n_samples} measurements have exceeded the UFR threshold of {self.UFR_long_TH} by nodes: {offender_names}",'orange',BACKGROUND=True)

            if self.verdict == VERDICT.UNKNOWN:
                outcome = color_html_element("...busy...", "blue")
                cause = "..."


        self.verdict_str = outcome
        self.verdict_cause_str = cause
        # self.append_queue_message({
        #     "MEASUREMENT OUTCOME": outcome,
        #     "CAUSE": cause
        # }, send=False, name="unknown")

    def set_alarms_to0(self):
        for server_index,a in enumerate(self.alarms):
            self.alarms[server_index]['UFR_short'] = False
            self.alarms[server_index]['UFR_long'] = False
            self.alarms[server_index]['index'] = False

    def check_for_UFR_alarms(self, name, UFR_short, UFR_long):
        UFR_short_alarm, UFR_long_alarm = False, False
        if UFR_short >= self.UFR_short_TH:
            msg = "CROSSED THE SHORT THRESHOLD! TIME:[{}] name:[{}] UFR[{}] >= Threshold[{}]\n".format(
                get_current_time()['str'], name, UFR_short, self.UFR_short_TH)
            self.log_message(msg)
            UFR_short_alarm = True

        if UFR_long >= self.UFR_long_TH:
            msg = "CROSSED THE LONG THRESHOLD! TIME:[{}] name:[{}] UFR[{}]>Threshold[{}]\n".format(
                get_current_time()['str'], name, UFR_long, self.UFR_long_TH)
            self.log_message(msg)
            UFR_long_alarm = True

        return UFR_short_alarm, UFR_long_alarm

    def get_data_over_ssh(self, force_new_login=False):
        try:
            if self.ssh.is_alive() and (force_new_login == False):
                print("SSH connection is alive")
            else:
                print(C_WARNING + "...Setting up SSH connection..." + C_END)
                self.ssh.open_ssh(self.server, self.username, self.password)
                self.get_system_information()

            cntr = 0
            while cntr < 2:
                output = self.ssh.send_cmd(cmd_gci)
                data_out = parse_node_info(output)
                cntr += 1
                if (data_out != {}):
                    break
                print("CMD not parsed well.. trying again")

            if data_out == {}:
                return False

            return data_out

        except Exception as e1:
            print(e1)
            try:  ##again!
                self.ssh.open_ssh(self.server, self.username, self.password)
            except Exception as e2:
                print(C_ALARM + "FAILED RE-OPENING SSH CONNECTION" + C_END)

                self.append_queue_message({
                    "FAIL1": color_html_element(f"{e1} {self.username}@{self.server}", "red"),
                    "FAIL2": color_html_element(f"{e2} {self.username}@{self.server}", "red")},
                    send=True)
                return False
        return False

    def get_system_information(self):
        system_info = self.ssh.send_cmd(cmd_gsi)
        system_info_out = list(parse_node_info(system_info).values())[0]
        self.SI_serialnumber = system_info_out['SerialNumber']
        self.SI_softwareversion = system_info_out['SoftwareVersion']

    def add_UFR_info_to_data(self):

        s_max = int(self.n_samples)  # sample max index
        for key in self.sshout_dict.keys():
            if key in self.connection_info_dict:

                for key2 in self.sshout_dict[key].keys():
                    if key2 in self.connection_info_dict[key]:
                        self.connection_info_dict[key][key2] = self.sshout_dict[key][key2]
                    else:
                        self.connection_info_dict[key].update({key2, self.sshout_dict[key][key2]})

                self.connection_info_dict[key]['total hist'].insert(0, self.sshout_dict[key]['FECBlockTotal(packets)'])
                self.connection_info_dict[key]['total hist'] = self.connection_info_dict[key]['total hist'][0:s_max]

                self.connection_info_dict[key]['lost hist'].insert(0, self.sshout_dict[key][
                    'FECBlockUncorrected(packets)'])
                self.connection_info_dict[key]['lost hist'] = self.connection_info_dict[key]['lost hist'][0:s_max]
            else:
                self.connection_info_dict.update({key: self.sshout_dict[key]})
                self.connection_info_dict[key]['total hist'] = [self.sshout_dict[key]['FECBlockTotal(packets)'] for i in
                                                                range(s_max)]
                self.connection_info_dict[key]['lost hist'] = [self.sshout_dict[key]['FECBlockUncorrected(packets)'] for
                                                               i in range(s_max)]

    def log_message(self, message):
        file = open(self.filename, "a")
        file.write(message)
        file.close()

    def append_queue_message(self, data, send=False, name='NONE'):
        if name in self.HTML_output_dict.keys():
            self.HTML_output_dict[name].update(data)
        else:
            self.HTML_output_dict[name] = data

        if send:
            self.send_queue_message()

    def send_queue_message(self):
        self.queue_out.put(self.HTML_output_dict)

    def update_cumulative_UFR(self, direction, UFR_short, UFR_long, UFR_forever, l1max):

        if direction == 'upstream':
            self.cumulative_UFR['US_short'] += UFR_short
            self.cumulative_UFR['US_long'] += UFR_long
            self.cumulative_UFR['US_forever'] += UFR_forever
            self.cumulative_UFR['US_lowest_l1max'] = min(self.cumulative_UFR['US_lowest_l1max'], l1max)
            return
        if direction == 'downstream':
            self.cumulative_UFR['DS_short'] += UFR_short
            self.cumulative_UFR['DS_long'] += UFR_long
            self.cumulative_UFR['DS_forever'] += UFR_forever
            self.cumulative_UFR['DS_lowest_l1max'] = min(self.cumulative_UFR['DS_lowest_l1max'], l1max)
            return

    def reset_cumulative_UFR(self):
        self.cumulative_UFR = {'US_short': 0, 'DS_short': 0, 'US_long': 0, "US_lowest_l1max": 999999,
                               'DS_long': 0, 'US_forever': 0, 'DS_forever': 0, "DS_lowest_l1max": 999999}


# def run_ssh_logger():
#

if __name__ == "__main__":

    queues = {'out': Queue(), 'in': Queue(), 'service': Queue()}
    server = "10.1.200.32"
    username = "xxx"
    password = "xxx"
    sample_period = 1
    n_samples = 12
    UFR_short_TH = 1.5
    UFR_long_TH = 2.5

    longopts = ['ip=', 'name=', 'password=', 'speriod=', 'nsamples=', 'th1=', 'thn=', 'indefinite_mode=']


    def get_input_options():
        argv = sys.argv[1:]
        opts, args = getopt.getopt(argv, 'a:b:c', longopts)
        return opts


    opts = get_input_options()

    for op in opts:
        if op[0] == '--ip':
            server = op[1]
        if op[0] == '--name':
            username = op[1]
        if op[0] == '--password':
            password = op[1]
        if op[0] == '--speriod':
            sample_period = int(op[1])
        if op[0] == '--nsamples':
            n_samples = int(op[1])
        if op[0] == '--th1':
            UFR_short_TH = float(op[1])
        if op[0] == '--thn':
            UFR_long_TH = float(op[1])
        if op[0] == '--indefinite_mode':
            if int(op[1]):
                mmode0 = MMODE_I
            else:
                mmode0 = MMODE_D

    ssh = SSH_LOGGER(queues=queues,
                     server=server,
                     username=username,
                     password=password,
                     UFR_long_TH=UFR_long_TH,
                     UFR_short_TH=UFR_short_TH,
                     sample_period=sample_period,
                     n_samples=n_samples)
