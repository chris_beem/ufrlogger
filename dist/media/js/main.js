$(document).ready(function() {

    function get_uptime_info()
    {
        $.post('/JSONget_global_system_information',{},function(data)
        {
            data = jQuery.parseJSON(data)

            var starttime = String(data.starttime);
            var uptime = String(data.uptime);
            var servers = String(data.servers);
            var sample_period = String(data.sample_period);
            var n_samples = String(data.n_samples);
            var mmode = 'Continuous measurement (No outcome)'
            if (parseInt(data.mmode) == 0)
            {
                mmode = 'DEFINITE (with outcome)'
            }
            var th1 = String(data.th1);
            var thn = String(data.thn);

            tabledata = [['starttime',
            'uptime',
            'servers polled',
            'sample period',
            'Number of samples',
            'Measurement mode']
            ,[starttime,uptime,servers,sample_period,n_samples,mmode]]


            style = "style = \"border: 1px solid gray; width: 60%;margin: auto; font-size: 16px;\""
            create_table($('.global_information'),tabledata,style)
        });
    }

    function create_table(element,data,style)
    {
        var table_str = "<table "+style+">"



                $.each(data[0],function(index,value){
                table_str = table_str.concat( '<tr>' );
                table_str = table_str.concat( '<th style="border: 1px solid gray;">' + data[0][index] + '</th>' );
                table_str = table_str.concat( '<td style="border: 1px solid gray;">' + data[1][index] + '</td>' );
                table_str = table_str.concat( '</tr>' );
                });



        table_str = table_str.concat('</table>')
        //console.log(table_str)
        element.html(table_str);

    }

    function get_system_info()
    {
        table_style = style = "style = \"border: 1px solid gray; width: 33%; font-size: 16px;\""
                    $.post('/JSONget_systeminfo',{},function(data)
                    {
                        data = jQuery.parseJSON(data)

                        //console.log('.SYSTEM_INFO_SN#'+data);

                        $.each(data,function(index,value){
                                    var cm_sn = value.cm_sn
                                    var cm_sv = value.cm_sv
                                    var samples_taken = value.samples_taken
                                    var ip = value.ip
                                    var verdict = value.verdict

                                    //console.log("i: ->"+index+" "+cm_sn+" "+cm_sv+" "+samples_taken+ ' '+value+' '+ip)

                                    var tdata = [['IP address','CM Serial Number','SW version', 'Sample state', 'Measurement outcome'],[ip,cm_sn,cm_sv,samples_taken,verdict]]


                                    create_table($('.SYSTEM_INFO[name='+index+']'),tdata,table_style);
                        });
                    });
    }


    function init_iframes()
    {
        $.post('/JSONget_setup', {}, function(data) {

            data = jQuery.parseJSON(data);
            servers = data.servers;
            sps = data.sample_period
            spl = data.n_samples
            mmode = parseInt(data.mmode)

            $.each(servers, function(index, value) {
                //border:1px solid black;
                var str = " ";
                str = str.concat("<td> <div style=\"text-align:center;\">")
                str = str.concat('<div class="SYSTEM_INFO" name='+String(index)+'  style=\"margin:auto;\" > ...loading... </div> ')
                str = str.concat('<iframe class="biframe" id=' + String(index) + ' src="/frame?i=' + String(index) + ' " height=1000 width=960  frameborder="2" allowfullscreen=""></iframe>');
                str = str.concat("<div> </td>")

                $("#iframes_table").append(str)
            });

            $('#overview_servers').html(String(servers))
            $('#overview_sps').html(String(sps))
            $('#overview_spl').html(String(spl))
            if(mmode == 1)
            {
                $("#mmode").html('Continuous measurement (No outcome)')
            }
            else
            {
                $("#mmode").html('DEFINITE (with outcome)')
            }

        });



    }



    function update_iframes()
    {
        $('.biframe').each(function(data) {
            this.reload = true;
            this.src = this.src;
            $(this).show();
        });
    }

    function period()
    {
        //update_iframes();
        get_uptime_info();
        get_system_info();
        setTimeout(period, 1000);
    };




    $('#SUBMIT_RESTART').on("click", function(){

        if (confirm('Do you want to restart?'))
        {
        $.post('/JSONset_restart', {});

            setTimeout(function() {
            location.reload();
            }, 5000);
        }
    });


    $('#SUBMIT_INIT').on("click", function()
    {
        var servers = []
        var usernames = []
        var passwords = []
        var sample_period = 0
        var n_samples = 0
        var UFR_short_TH = []
        var UFR_long_TH = []
        var mmode = 0

        $(".INIT_IP").each(function() { servers.push(String($(this).val())) });
        $(".INIT_UNAMES").each(function() { usernames.push(String($(this).val())) });
        $(".INIT_PASS").each(function() { passwords.push(String($(this).val())) });
        $(".INIT_UFR_TH_S").each(function() { UFR_short_TH.push(String($(this).val())) });
        $(".INIT_UFR_TH_L").each(function() { UFR_long_TH.push(String($(this).val())) });

        sample_period = $(".INIT_SAMPLE_PERIOD").val()
        n_samples = $(".INIT_N_SAMPLES").val()
        if($("[name = INIT_MMODE]:checked").val() == 'INDEFINITE')
        {
            mmode = 1
        }
        else
        {
            mmode = 0
        }




        $.post('/JSONset_setup', {
            servers:JSON.stringify(servers),
            usernames:JSON.stringify(usernames),
            passwords:JSON.stringify(passwords),
            sample_period:parseInt(sample_period),
            n_samples:parseInt(n_samples),
            UFR_short_TH:JSON.stringify(UFR_short_TH),
            UFR_long_TH:JSON.stringify(UFR_long_TH),
            mmode:parseInt(mmode)
        }, function(data) {
            $(".before_init").hide();
            $(".after_init").show();
            init_iframes();
        });
    });

    function get_setup() {
        $.post('/JSONget_setup', {} , function(data){
            data = jQuery.parseJSON(data)

            if(data.inited == true)
            {
                $(".before_init").hide();
                $(".after_init").show();
                init_iframes();
                return;
            }

            $(".after_init").hide();

            //console.log("SETUP0 DATA:",data,data.usernames.length);

            var nservers = data.servers.length;
            var nusernames= data.usernames.length;
            var npasswords= data.passwords.length;
            var nUFR_short_THs= data.UFR_short_THs.length;
            var nUFR_long_THs= data.UFR_long_THs.length;
            var mmode = data.mmode;

            $.each(data.servers,function(index,value){$('.INIT_IP[name='+parseInt(index)+']').val(value)});

            if(nusernames>1)
            {$.each(data.usernames,function(index,value){$('.INIT_UNAMES[name='+parseInt(index)+']').val(value)});}
            else
            {for(var index=0;index<nservers;index++){$('.INIT_UNAMES[name='+parseInt(index)+']').val(data.usernames[0])}}

            if(npasswords>1)
            {$.each(data.passwords,function(index,value){$('.INIT_PASS[name='+parseInt(index)+']').val(value)});}
            else
            {for(var index=0;index<nservers;index++){$('.INIT_PASS[name='+parseInt(index)+']').val(data.passwords[0])}}

            if(nUFR_short_THs>1)
            {$.each(data.UFR_short_THs,function(index,value){$('.INIT_UFR_TH_S[name='+parseInt(index)+']').val(value)});}
            else
            {for(var index=0;index<nservers;index++){$('.INIT_UFR_TH_S[name='+parseInt(index)+']').val(data.UFR_short_THs[0])}}

            if(nUFR_long_THs>1)
            {$.each(data.UFR_long_THs,function(index,value){$('.INIT_UFR_TH_L[name='+parseInt(index)+']').val(value)});}
            else
            {for(var index=0;index<nservers;index++){$('.INIT_UFR_TH_L[name='+parseInt(index)+']').val(data.UFR_long_THs[0])}}

            $('.INIT_SAMPLE_PERIOD').val(data.sample_period)
            $('.INIT_N_SAMPLES').val(data.n_samples)


            if(mmode == 1)
            {
                $("#INDEFINITE[name=INIT_MMODE]").prop('checked',true)
                $("#DEFINITE[name=INIT_MMODE]").prop('checked',false)
            }
            else
            {
                $("#INDEFINITE[name=INIT_MMODE]").prop('checked',false)
                $("#DEFINITE[name=INIT_MMODE]").prop('checked',true)
            }


        });}


        get_setup();
        period();
});