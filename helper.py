import pip
import importlib
import os
import pip
from time import sleep
import time
from queue import Queue
import threading
import os
import datetime
import sys
import getopt
from decimal import Decimal

os.system("")

#longopts = ['ip=', 'name=', 'password=', 'sleep=', 'TH5=', 'TH60=']

class VERDICT():
    PASS = 1
    FAIL = 2
    UNKNOWN = 0

MMODE_I = 1 #MEASUREMENT INDEFINITE MODE
MMODE_D = 0 #MEASUREMENT DEFINITE MODE (with verdict)

C_HEADER = '\x1b[0;30;42m'
C_WARNING = '\x1b[0;30;43m'
C_ALARM = '\x1b[0;30;41m'
C_END = '\x1b[0m'

DIR_COL_US = '#d1e3e2'
DIR_COL_DS = '#d1d1e2'

def color_html_element(s,color,BACKGROUND = False):
    if BACKGROUND:
        return """<p style="background-color:{}";>""".format(color)+str(s)+"""</p>"""
    return """<p style="color:{}";>""".format(color)+str(s)+"""</p>"""

def dct_to_html(dct):
    st = "<table>"
    et = "</table>"
    sh = "<th>"
    eh = "</th>"
    sr = "<tr name>"
    er = "</tr>"
    sc = "<td>"
    ec = "</td>"

    html = []
    html.append(st)
    headers = ['']

    for key1,val1 in dct.items():

        UPDATE_HEADERS = (headers != list(val1.keys()))

        if UPDATE_HEADERS: #IF headers do not corrospond to previous headers, add new header field, otherise use old headers
            headers = list(val1.keys())
            html.append(sr)
            for new_header in headers:
                html.append(sh)
                html.append(new_header)
                html.append(eh)
            html.append(er)

        html.append(sr.replace('name',key1))
        for key2,val2 in val1.items(): #add values under the headers/new headers
            val2 = str(val2)
            html.append(sc)
            html.append(val2)
            html.append(ec)
        html.append(er)

    html.append(et)
    html = [str(item) for item in html]
    html = "".join(html)
    return html

def get_HREFs_from_folder(folder):
    files = os.listdir(folder)

    markup = """<table>{base}</table>"""
    base_href = """<tr><td><a href="{map}/////////{file}">{link_text}</a></td></tr>"""
    link_tags = [base_href.format(map = folder,
                                  file = f,
                                  link_text=f) for i,f in enumerate(files)]
    link_tags = ''.join(link_tags)
    return markup.format(base=link_tags)

def get_current_time():
    now = datetime.datetime.now()
    return {'value':now,'str':now.strftime("%m/%d/%Y, %H:%M:%S")}

def get_time_difference(t1,t2):
    seconds = (t2 - t1).total_seconds()

    hours = seconds // (60*60)
    seconds %= (60*60)
    minutes = seconds // 60
    seconds %= 60
    hours = int(hours)
    minutes = int(minutes)
    seconds = int(seconds)

    if(hours>0):
        return f"{hours}h, {minutes}m, {seconds}s"
    elif minutes > 0:
        return f"{minutes}m, {seconds}s"
    else:
        return f"{seconds}s"


#get_HREFs_from_folder("media")

def sc_notation(number):
    if number == 0:
        return 0

    return float("{:.2E}".format(Decimal(number)))

def install(package):
    if hasattr(pip, 'main'):
        pip.main(['install', package])
    else:
        pip._internal.main(['install', package])

def try_import_or_install(package):
    try:
        importlib.import_module(package)
        #print(f"{package} imported!")
    except ImportError as e:
        print(f"package {package} missing, attemping to install!")
        install(package)
        importlib.import_module(package)
        pass  # module doesn't exist, deal with it.

try_import_or_install("cherrypy")
import cherrypy

try_import_or_install("paramiko")
import paramiko

try_import_or_install("json")
import json

try_import_or_install("subprocess")
import subprocess